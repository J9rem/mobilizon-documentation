# Development

Start by retrieving the code by cloning the repository:

=== "HTTPS"
    ``` bash
    git clone https://framagit.org/framasoft/mobilizon && cd mobilizon
    ```

=== "SSH"
    !!! info
        This requires to have an account on [Framagit](https://framagit.org), our self-hosted Gitlab code forge. You may use the following URI instead of anonymous access: `git://framagit.org/framasoft/mobilizon`, but you won't be able to push any changes that way.

    ``` bash
    git clone git@framagit.org:framasoft/mobilizon.git && cd mobilizon
    ```

Run Mobilizon:

  + with Docker and Docker Compose (**Recommended**)
  + without Docker and Docker Compose (This involves more work on your part, use Docker and Docker Compose if you can)

## With Docker

  + Copy `.env.template` to `.env` and tweak it to your likening
  + Install `Make`
  + Install [Docker](https://docs.docker.com/install/#supported-platforms) and [Docker Compose](https://docs.docker.com/compose/install/) for your system.  
     Note: [Podman](https://podman.io) versions ≥ 3 should work as well with the `docker-compose` command (instead of `docker compose`).
  + Run `make setup` to build, then `make` to launch a database container and an API container.
  + Follow the progress of the build with `make logs`.
  + Access [`localhost:4000`](http://localhost:4000) in your browser once the containers are fully built and launched.
  + Stop everything with `make stop`

### Mix tasks

For instance creating an admin user

```bash
docker compose exec api mix mobilizon.users.new "your@email.com" --admin --password "mypassword"
```

All other Mix commands ([from Mobilizon](../administration/CLI tasks/index.md) and dependencies) are available in the same way. For instance to generate a new migration:
```bash
docker compose exec api mix ecto.gen.migration MyMigrationModuleName
```
Then once it's written
```bash
docker compose exec api mix ecto.migrate
```
(or the shortcut version `make migrate`)

If you need to execute the `iex` while running, you can launch the database container on it's own:
```
docker compose start postgres
```
And then run the API container with `iex`:
```sh
docker compose run --service-ports --rm api iex -S mix phx.server
```

## Without Docker


### Setup

#### Install system dependencies

You may follow the same guide as for [installing dependencies in source mode](../administration/install/source.md#dependencies).

- [Elixir (and Erlang)](https://elixir-lang.org/install.html)
- PostgreSQL ≥ 11 with PostGIS
- [Install NodeJS](https://nodejs.org/en/download/) (we guarantee support for the latest LTS) ![](https://img.shields.io/badge/node-%3E%3D%2018+-brightgreen.svg)
- `cmake`, `inotify-tools`, ImageMagick, webp and basic build tools

#### Configure the database

Make sure the service is running.

Create a postgres user with database creation capabilities. For instance:

```sh
sudo -i -u postgres
createuser -d -P mobilizon
``` 
Set `mobilizon` as the password, then `exit`.

#### Install Mobilizon dependencies
 - Fetch backend Elixir dependencies with `mix deps.get` (you may need to accept prompts)
 - Compile Mobilizon dependencies with `mix deps.compile` (this can take a while and you may need to accept prompts)
 - Fetch front-end dependencies with `yarn --cwd "js"`
 - Compile front-end with `yarn --cwd "js" run build` (this can take a while)

#### Configure
 - Create your database with `mix ecto.create`
 - Creating extensions on the database requires a PostgreSQL user with superuser capacities. For instance:
  ```
  sudo -i -u postgres
  psql mobilizon_dev
  ```
  Then 
  ```psql
  create extension if not exists postgis;
  create extension if not exists unaccent;
  create extension if not exists pg_trgm;
  ```
  And finally exit both `psql` and the `postgres` system user.

- Run database migrations: `mix ecto.migrate`
- Generate configuration `mix mobilizon.instance gen`. Answer the following for these questions:
    * domain: `localhost`
    * database host: `localhost` (default)
    * database name: `mobilizon_dev`
    * database username: `mobilizon` (default)
    * database password: `mobilizon` (or whatever you've chosen before)

The rest can be left to default is possible or be set to whatever you prefer.

!!! tip
    To make sure URLs are generated properly, edit `config/runtime.exs` and add `, port: 4000` behind `url: [host: "localhost"`.

#### Generate first user
Generate your first user with the `mix mobilizon.users.new` task

```bash
mix mobilizon.users.new john.doe@localhost.com --admin
```

You should have a similar output:
```
A user has been created with the following information:
  - email: john.doe@localhost.com
  - password: r/EKpKr5o7ngQY+r
  - Role: user
The user will be prompted to create a new profile after login for the first time.
```

#### Start the server

Start Phoenix endpoint with `mix phx.server`. The client development server will also automatically be launched and will reload on file change.
Now you can visit [ `localhost:4000` ](http://localhost:4000) in your browser
and see the website (server *and* client) in action.

If you want to access `iex` while the server is running, you can use this command to launch the server:
```
iex -S mix phx.server
```

## FAQ

### Issues with argon2 when creating users.

This is because you installed deps through Docker and are now using Mobilizon without it, or the other way around. Just `rm -r deps/argon2_elixir` and trigger `mix deps.get` again.
