# Authenticating with the API

The API provides an oAuth2 compatible authentication and authorization method. Both the Authorization Code and Device Code grant types are supported.

## Create an app

Send an URL-encoded POST request to `/apps` to create an app.
The parameters are the following:

| Query parameter | Type   | Description                                                        |
| --------------- | ------ | ------------------------------------------------------------------ |
| `name`          | string | The app name                                                       |
| `redirect_uri`  | string | The redirect URIs, separated by new lines                          |
| `website`       | string | A website presenting your app                                      |
| `scope`         | string | The total list of scopes your app may ask for, separated by spaces |

Example:

```http
POST https://mobilizon.fr/apps
content-type: application/x-www-form-urlencoded

name=Mobilizon+Event+Importer&redirect_uris=https%3A%2F%2Fmyapp.com%2Fcallback%0Ahttps%3A%2F%2Fmyapp.com%2Fcallback2&website=https%3A%2F%2Fjoinmobilizon.org&scope=write%3Aevent%3Acreate+write%3Aevent%3Aupdate+write%3Aevent%3Adelete
```

It returns a JSON-encoded payload with your submitted data plus `client_id`, which identifies your app, and `client_secret`, which will be used in some cases later. Please note the values of `client_id` and `client_secret` somewhere as they will both be used later.

| Response parameter | Type   | Description                            |
| ------------------ | ------ | -------------------------------------- |
| `client_id`        | string | An unique ID to identify your app      |
| `client_secret`    | string | A secret known only by you             |
| `redirect_uri`     | string | The redirect uris allowed for your app |
| `scope`            | string | The scope provided                     |
| `website`          | string | The website for the app                |

Example successful response.

```json
{
  "client_id": "D4X8v1zmNrVMjukcPy4ceN/T1phWOyhXtHNuyMsQBw",
  "client_secret": "b6lBDMUN+Q8hecHKNRVxCHDGasLELGWbZ+vgFqV7s3",
  "name": "Mobilizon Event Importer",
  "redirect_uri": ["https://myapp.com/callback", "https://myapp.com/callback2"],
  "scope": "write:event:create write:event:update write:event:delete",
  "website": "https://joinmobilizon.org"
}
```

## Authorization Code grant type

### Authorizing the app

Send the user to GET `/oauth/authorize` to authorize your app. The following query parameters are required:

| Query parameter | Type   | Description                                                                                                                                                                    |
| --------------- | ------ | ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------ |
| `client_id`     | string | The `client_id` you got earlier.                                                                                                                                               |
| `redirect_uri`  | string | The callback URI you want to be redirected to after the user authorized your app. It needs to be included in the `redirect_uri` value you provided at the creation of the app. |
| `scope`         | string | The list of scopes you need the user to authorize, separated by spaces. Scopes needs to be included in (or equal to) the scopes list you provided at the creation of the app.  |
| `state`         | string | A random string of your choice to protect against forgery attacks. You will be able to check it hasn't been tampered with after the callback.                                  |

After the user approves your app, it will be redirected to the value of `redirect_uri` with the following query parameters:

| Response parameter | Type   | Description                                                              |
| ------------------ | ------ | ------------------------------------------------------------------------ |
| `code`             | string | The authorization code provided, that we will use just below             |
| `client_id`        | string | The client ID you provided                                               |
| `scope`            | string | The scope provided and authorized by the user                            |
| `state`            | string | The state parameter you provided, make sure it hasn't been tampered with |

### Exchanging the authorization code for tokens

Submit a POST request to `/oauth/token` with the following parameters:

| Query parameter | Type   | Description                                                      |
| --------------- | ------ | ---------------------------------------------------------------- |
| `code`          | string | The authorization code you just got earlier                      |
| `client_id`     | string | The client ID from your app                                      |
| `client_secret` | string | The client secret from your app                                  |
| `grant_type`    | string | Must be equal to `authorization_code` for this grant type        |
| `redirect_uri`  | string | The redirect URI provided from the initial authorization request |
| `scope`         | string | The scope provided from the initial authorization request        |

Mobilizon will return a JSON-encoded payload with in particular values for `access_token` and `refresh_token`.

Response parameters:

| Response parameter         | Type    | Description                                                                                          |
| -------------------------- | ------- | ---------------------------------------------------------------------------------------------------- |
| `access_token`             | string  | The access token                                                                                     |
| `expires_in`               | integer | The number of seconds until `access_token` expires. The value will always be `28800` (8 hours).      |
| `refresh_token`            | string  | The refresh token                                                                                    |
| `refresh_token_expires_in` | integer | The number of seconds until `refresh_token` expires. The value will always be `15811200` (3 months). |
| `scopes`                   | string  | The list of scopes this token is allowed to use                                                      |
| `token_type`               | string  | The token type will always be of type `bearer`                                                       |

The access token can now be used through the `Authorization` header like the following: `Authorization: Bearer [access_token]`.

Example successful response:

```json
{
  "access_token": "eyJhbGciOiJIUzUxMiIsInR5cCI6IkpXVCJ9.eyJhdWQiOiJtb2JpbGl6b24iLCJleHAiOjE2Nzc4ODk5NDEsImlhdCI6MTY3Nzg2MTE0MSwiaXNzIjoibW9iaWxpem9uIiwianRpIjoiYTVkMDU4NGUtMTY2My00OTc3LTk1N2UtYjZjZjk1ZTlmYzgxIiwibmJmIjoxNjc3ODYxMTQwLCJzdWIiOiJBcHBUb2tlbjoxMiIsInR5cCI6ImFjY2VzcyJ9.7D6FPNRmSSZ8HDYh0GT12G_cPSF-UH_XfpQU1dGiOa9ADULciiQIKYnsrYP0QVrKnSWLfdP06d0SiCksQTSjKQ",
  "expires_in": 28800,
  "refresh_token": "eyJhbGciOiJIUzUxMiIsInR5cCI6IkpXVCJ9.eyJhdWQiOiJtb2JpbGl6b24iLCJleHAiOjE2OTM1ODU5NDEsImlhdCI6MTY3Nzg2MTE0MSwiaXNzIjoibW9iaWxpem9uIiwianRpIjoiMTQ1MDk3NGMtOWE4NS00YmI3LWE5NTktODg5OGM5NzQxM2UzIiwibmJmIjoxNjc3ODYxMTQwLCJzdWIiOiJBcHBUb2tlbjoxMiIsInR5cCI6InJlZnJlc2gifQ.Ttn1PlqV3RZkewdwM-EIEgJwN8XTWB_PtmYSTkVCb7U7TrLqF5pVnN1brubj_ePLFohweLaJWah6X5ajNFNdrg",
  "refresh_token_expires_in": 15724800,
  "scopes": "write:event:create write:event:update write:event:delete",
  "token_type": "bearer"
}
```

## Device Code grant type

### Authorizing the app

#### Step 1: App requests the device and user verification codes from Mobilizon

Submit a POST request to `/login/device/code` with the following parameters:

| Query parameter | Type   | Description                                                                                                                                                                   |
| --------------- | ------ | ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| `client_id`     | string | The `client_id` you got earlier.                                                                                                                                              |
| `scope`         | string | The list of scopes you need the user to authorize, separated by spaces. Scopes needs to be included in (or equal to) the scopes list you provided at the creation of the app. |

The response will be a JSON-encoded payload with the following values:

| Response parameter | Type    | Description                                                                                                                                                                                                                                                                                                                                                           |
| ------------------ | ------- | --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| `device_code`      | string  | The device verification code is 40 characters and used to verify the device.                                                                                                                                                                                                                                                                                          |
| `expires_in`       | integer | The number of seconds before the device_code and user_code expire. The default is 900 seconds or 15 minutes.                                                                                                                                                                                                                                                          |
| `interval`         | integer | The minimum number of seconds that must pass before you can make a new access token request (POST `/oauth/token`) to complete the device authorization. For example, if the interval is 5, then you cannot make a new request until 5 seconds pass. If you make more than one request over 5 seconds, then you will hit the rate limit and receive a slow_down error. |
| `user_code`        | string  | The user verification code is displayed on the device so the user can enter the code in a browser. This code is 8 characters with a hyphen in the middle.                                                                                                                                                                                                             |
| `verification_uri` | string  | The verification URL where users need to enter the user_code: `/login/device`.                                                                                                                                                                                                                                                                                        |

Example successful response:

```json
{
  "device_code": "FWMWGMPNCCNUPCWBKNNNPBGYUJZGRUDGRVMOQJQM",
  "expires_in": 900,
  "interval": 5,
  "user_code": "MZQO-TGIC",
  "verification_uri": "https://mobilizon.fr/login/device"
}
```

#### Step 2: Prompt the user to enter the user code in a browser

Your device will show the user verification code and prompt the user to enter the code at `https://my-mobilizon-instance.tld/login/device`.

#### Step 3: App polls Mobilizon to check if the user authorized the device

Your app will make device authorization requests that poll POST `/oauth/token`, until the device and user codes expire or the user has successfully authorized the app with a valid user code. The app must use the minimum polling interval retrieved in step 1 to avoid rate limit errors.

The user must enter a valid code within 15 minutes (or 900 seconds). After 15 minutes, you will need to request a new device authorization code with POST `/login/device/code`.

Once the user has authorized, the app will receive an access token that can be used to make requests to the API on behalf of a user.

The endpoint takes the following input parameters.

| Query parameter | Type   | Description                                                                          |
| --------------- | ------ | ------------------------------------------------------------------------------------ |
| `client_id`     | string | The `client_id` you got earlier.                                                     |
| `device_code`   | string | The device verification code you received from the `POST /login/device/code` request |
| `grant_type`    | string | The grant type must be `urn:ietf:params:oauth:grant-type:device_code`.               |

Mobilizon will return a JSON-encoded payload with in particular values for `access_token` and `refresh_token`.

Response parameters:

| Response parameter         | Type    | Description                                                                                          |
| -------------------------- | ------- | ---------------------------------------------------------------------------------------------------- |
| `access_token`             | string  | The access token                                                                                     |
| `expires_in`               | integer | The number of seconds until `access_token` expires. The value will always be `28800` (8 hours).      |
| `refresh_token`            | string  | The refresh token                                                                                    |
| `refresh_token_expires_in` | integer | The number of seconds until `refresh_token` expires. The value will always be `15811200` (3 months). |
| `scopes`                   | string  | The list of scopes this token is allowed to use                                                      |
| `token_type`               | string  | The token type will always be of type `bearer`                                                       |

The access token can now be used through the `Authorization` header like the following: `Authorization: Bearer [access_token]`.

Example successful response:

```json
{
  "access_token": "eyJhbGciOiJIUzUxMiIsInR5cCI6IkpXVCJ9.eyJhdWQiOiJtb2JpbGl6b24iLCJleHAiOjE2Nzc4ODk5NDEsImlhdCI6MTY3Nzg2MTE0MSwiaXNzIjoibW9iaWxpem9uIiwianRpIjoiYTVkMDU4NGUtMTY2My00OTc3LTk1N2UtYjZjZjk1ZTlmYzgxIiwibmJmIjoxNjc3ODYxMTQwLCJzdWIiOiJBcHBUb2tlbjoxMiIsInR5cCI6ImFjY2VzcyJ9.7D6FPNRmSSZ8HDYh0GT12G_cPSF-UH_XfpQU1dGiOa9ADULciiQIKYnsrYP0QVrKnSWLfdP06d0SiCksQTSjKQ",
  "expires_in": 28800,
  "refresh_token": "eyJhbGciOiJIUzUxMiIsInR5cCI6IkpXVCJ9.eyJhdWQiOiJtb2JpbGl6b24iLCJleHAiOjE2OTM1ODU5NDEsImlhdCI6MTY3Nzg2MTE0MSwiaXNzIjoibW9iaWxpem9uIiwianRpIjoiMTQ1MDk3NGMtOWE4NS00YmI3LWE5NTktODg5OGM5NzQxM2UzIiwibmJmIjoxNjc3ODYxMTQwLCJzdWIiOiJBcHBUb2tlbjoxMiIsInR5cCI6InJlZnJlc2gifQ.Ttn1PlqV3RZkewdwM-EIEgJwN8XTWB_PtmYSTkVCb7U7TrLqF5pVnN1brubj_ePLFohweLaJWah6X5ajNFNdrg",
  "refresh_token_expires_in": 15724800,
  "scopes": "write:event:create write:event:update write:event:delete",
  "token_type": "bearer"
}
```

## Authentication

### Access token

To access protected operations and contents, the access token must be added in the Authorization header of the HTTP request as the bearer.

Here is an example with `curl` command :

```bash
curl -sS \
  -H 'accept: application/json' \
  -H "Authorization: Bearer eyJXXXJ9.eyJxxxQ.SXXX5c" \
  -F "query=${gql}" \
  -F "variables=${vars}" \
  "${MOBILIZON_ENDPOINT}"
```

In graphiql playground, there is a button to add headers :

![graphiql headers](../images/en/graphiql_bearer.png)

Other HTTP clients may have other ways to setup the bearer.

### Refresh token

When access token is expired, protected operations will return a 401 code.

In that case, the access token must be renewed using the refresh token using the `refreshToken` mutation.

```graphql
mutation RefreshToken($rt: String!) {
  refreshToken(refreshToken: $rt) {
    accessToken
    refreshToken
  }
}
```

You get back an access token and a refresh token which replace the previous ones.
